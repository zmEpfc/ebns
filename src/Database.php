<?php

class Database
{
    private $host = "private";
    private $db_name = "private";
    private $username = "private";
    private $password = "private";
    public $conn;

    public function __construct()
    {
        $this->dbConnection();
    }

    public function dbConnection()
    {
        $this->conn = null;
        try
        {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->exec("SET NAMES 'utf8';");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exception)
        {
            //echo "Connection error: " . $exception->getMessage();
        }

        return $this->conn;
    }

    public function getDemandesConges()
    {
        try
        {
            $stmt = $this->conn->prepare("SELECT * FROM wp_sev_posts where post_title = 'Demande de congés'");
            $stmt->execute();
            $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $res;
        } catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function updateDemandesConges($acceptee)
    {

        try
        {
            $stmt = $this->conn->prepare("UPDATE wp_sev_posts SET acceptee = :decision where ID = :ID");
            $stmt->bindValue(':decision', $acceptee[0]);
            $stmt->bindValue(':ID', $acceptee[1]);
            $stmt->execute();
        } catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function updateComment($comment)
    {
        try
        {
            $stmt = $this->conn->prepare("UPDATE wp_sev_posts SET comment = :comment where ID = :ID");
            $stmt->bindValue(':comment', $comment[0]);
            $stmt->bindValue(':ID', $comment[1]);
            $stmt->execute();
        } catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }
}
