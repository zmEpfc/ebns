<?php
/**

 * Template Name: Custom PHP

 * @package craft

 */
get_header();
?>

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php
        require_once 'src/Database.php';
        require_once 'src/DemandeControlleur.php';
        
        /*
         *  convertie une décision 0 | 1 | 3 en format textul
         *  @param $decision : String
         *  return String
         */
        function decisionToString($decision)
        {
            switch($decision){
                case 0 :
                    $res = "Accepté";
                    break;
                case 1 :
                    $res = "En attente";
                    break;
                case 2 :
                    $res = "Refus";
                    break;
                default :
                    $res = "En attente";
            }
            return $res;
        }

        $pdo = new Database();
        $liste_demandes = $pdo->getDemandesConges();
        $demandes = [];
        $demandesCurrentUser = [];
        $userId = wp_get_current_user()->id;

        foreach ($liste_demandes as $demande)
        {
            $temp = [$demande['post_title']];
            //echo $demande['post_content'].'<br>';
            $t = explode(PHP_EOL, $demande['post_content']);
            $temp[] = $t;
            $temp[] = $demande['ID'];
            $temp[] = $demande['acceptee'];
            $temp[] = $demande['comment'];
            $demandes[] = $temp;

            //récuperation des demandes de l'user en cours. Qu'il soit manager ou employe on affiche ses demandes
            if (intval($t[4]) == $userId)
            {
                $temp2[] = $t;
                $temp2[] = $demande['acceptee'];
                $temp2[] = $demande['comment'];
                $demandesCurrentUser[] = $temp2;
                $temp2 = [];
            }
        }

        //récupération du role de user courant
        $userId = wp_get_current_user()->id;
        $arrRoles = wp_get_current_user()->roles;
        $is_manager = in_array('administrator', $arrRoles) || in_array('manager', $arrRoles);
        if ($is_manager)
        {
            ?>
            <table class="table table-responsive" data-ajax="<?php echo get_template_directory_uri(); ?>">
                <colgroup>
                    <col class="col-xs-2" span="3"></col>
                </colgroup>
                <thead>
                    <tr>
                        <th>Matricule Employé</th>
                        <th>Date début</th>
                        <th>Date fin</th>
                        <th class="text-center">Statut décision</th>
                        <th>Commentaire RH</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    foreach ($demandes as $k => $dem)
                    {
                        ?>
                        <tr>
                            <td><?php echo $dem[1][0] ?></td>
                            <td><?php echo $dem[1][2] ?></td>
                            <td><?php echo $dem[1][3] ?></td>
                            <td data-decision="<?php echo $dem[3] ?>" class="col-xs-12" data-id="<?php echo $dem[2] ?>">
                                <div class="btn-group" data-toggle="buttons" class="col-xs-12">
                                    <label class="btn btn-default">
                                        <input type="radio" name="options" class="decision" id="0" autocomplete="off"> Acceptée
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" name="options" class="decision" id="1" autocomplete="off"> En attente
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" name="options" class="decision" id="2" autocomplete="off"> Refusée
                                    </label>
                                </div>
                            </td>
                            <td data-id="<?php echo $dem[2] ?>">
                                <!--  Remplissage texteArea-->
                                <div class="form-group">
                                    <textarea class="form-control managerComment<?php echo $dem[2] ?>" rows="5"  name="comment"><?php
                                        if (!empty($dem[4]))
                                        {
                                            echo trim($dem[4]);
                                        } else
                                        {
                                            echo "En attente";
                                        }
                                        ?></textarea><br />

                                    <div class="text-center col-xs-12"><button type="button" class="btn btn-info btn">Envoyer commentaire</button></div>
                                    <div class="response-info col-xs-12"></div>
                                </div>
                            </td>
                        </tr>

                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
        } else if ($is_manager || !$is_manager)
        {
            echo "<h2>Vos demandes</h2>";
            ?>
            <table class = "table table-responsive" data-ajax = "<?php echo get_template_directory_uri(); ?>">
                <colgroup>
                    <col class = "col-xs-2" span = "3"></col>
                </colgroup>
                <thead>
                    <tr>
                        <th class="text-center">Date début</th>
                        <th class="text-center">Date fin</th>
                        <th class = "text-center">Décision</th>
                        <th class = "text-center">Commentaire RH</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($demandesCurrentUser as $demandeCurrent)
                    {

                        //print_r($demandeCurrent);
                        ?>
                        <tr>
                            <td><?php echo $demandeCurrent[0][2] ?></td>
                            <td><?php echo $demandeCurrent[0][3] ?></td>
                            <td><?php echo decisionToString($demandeCurrent[1]) ?></td>
                            <td><?php echo $demandeCurrent[2] ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
        }
        ?>
    </main><!-- #main -->
</div><!-- #primary -->
<script>
    jQuery(document).ready((function ($) {
        //distribution de la classe btn-primary au 1er chargement
        //pour chaque bouton radio vérifier si data-decision == $(this).attr(id)
        $('input:radio').each(function () {
            var decision = $(this).parent().closest('td').attr('data-decision');
            if ($(this).attr('id') == decision) {
                $(this).parent().closest('.btn-default').addClass('btn-primary');
            }
        });

        var url = $(".table").attr('data-ajax') + '/src/DemandeControlleur.php';
        /* envoie du commentaire manager */
        $('.btn-info').each(function () {
            $(this).on({
                'click': function () {
                    var demandeId = $(this).parent().closest('td').attr('data-id');
                    var managerComment = $('.managerComment' + demandeId).val();
                    window.console.log(managerComment);
                    var commentArr = [];
                    //on envoit le commentaire en bdd
                    commentArr.push(managerComment, demandeId);
                    var jdata = {
                        'comment': commentArr
                    };

                    $.post(url, {'comment': jdata}, function (result, status) {
                        if (status === 'success') {
                            alert('message envoyé !')
                        } else {
                            alert('Message non-envoyé');
                        }
                    });
                }
            });
        });

        $('.decision').each(function () {
            $(this).on({
                'change': function () {
                    // supprime la classe primary btn old
                    //$('.btn-primary').removeClass('btn-primary');
                    $(this).parent().closest('td').find('.btn-primary').removeClass('btn-primary');
                    //donner la classe primary btn new
                    $(this).parent().closest('.btn').addClass('btn-primary');

                    // préparation de la requete ajax
                    var acceptee = [];
                    var decision = $(this).prop('id');
                    var demandeId = $(this).parent().closest('td').attr('data-id');
                    acceptee.push(decision, demandeId);
                    var jdata = {
                        'acceptee': acceptee
                    };

                    $.post(url, {acceptee: jdata}, function (data, status) {
                        //console.log(status);

                    });
                }
            });
        });
    }));
</script>

<?php
// do_action('storefront_sidebar');

get_footer();
